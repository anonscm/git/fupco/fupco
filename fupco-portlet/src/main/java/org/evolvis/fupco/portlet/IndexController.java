/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.portlet;


import org.evolvis.fupco.UploadedFile;
import org.evolvis.fupco.Validator;
import org.evolvis.fupco.checks.Check;
import org.evolvis.fupco.config.Configuration;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.portlet.PortletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import javax.portlet.ActionRequest;
import java.util.List;

@Controller
@RequestMapping(value = "view")
public class IndexController {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ActionMapping
    public void doGetUserInput1(ActionRequest request, ModelMap model) {
        logger.info("upload-file-action triggered");

        FileItemFactory factory = new DiskFileItemFactory();
        PortletFileUpload upload = new PortletFileUpload(factory);

        Configuration config;
        Validator validator = new Validator();
        try {
            config = Configuration.readConfig(getClass().getResource("/fupco-config.xml").getFile());

            List<FileItem> items = upload.parseRequest(request);

            for (FileItem item : items) {
                if (item.isFormField()) {
                    continue;
                }
                logger.info("file found in request: " + item.getName());
                UploadedFile uploadedFile = validator.validate(item.getInputStream(), config);

                if (!uploadedFile.isValidationSuccess()) {
                    Check failedCheck = uploadedFile.getFailedCheck();
                    String message = "check: " + failedCheck.getClass().getName() + " failed with: " + failedCheck.getCheckResult().getMessage();
                    logger.info(message);
                    model.addAttribute("validationResult", message);
                } else {
                    logger.info("file accepted");
                    model.addAttribute("validationResult", "successful");
                }
            }
        } catch (Exception e) {
            logger.error("an exception occurred", e);
            model.addAttribute("error", e.getMessage());
        }
    }

    @RenderMapping
    public String getJsp() {
        return "index";
    }
}
