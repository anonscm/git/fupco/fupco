<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>

    <c:if test='${error!=null}'>
            ${error}
    </c:if>

    <portlet:actionURL var="formAction" windowState="normal"/>
    <form action="${formAction}" id="pageForm" method="post" enctype="multipart/form-data">
        <input name="uploadfile" type="file"/>
        <input type="submit"/>
    </form>

    <c:if test='${validationResult!=null}'>
            ${validationResult}
    </c:if>
