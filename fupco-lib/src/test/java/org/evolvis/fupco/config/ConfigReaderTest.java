/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.config;

import org.evolvis.fupco.checks.Check;
import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * a test class for {@link ConfigReader}
 *
 * @author hgross
 */
public class ConfigReaderTest {
    private final static String pathToInvalidClassConfig = "src/test/resources/InvalidClassConfig.xml";
    private final static String pathToInvalidXMLConfig = "src/test/resources/InvalidXmlConfig.xml";
    private final static String pathToInvalidFormatConfig = "src/test/resources/InvalidFormatConfig.xml";
    private final static String pathToInvalidTypeConfig = "src/test/resources/InvalidTypeConfiguration.xml";
    private final static String pathToConfigFile = "src/test/resources/ConfigReaderTestConfig.xml";


    @Test
    public void test_no_config_file_found() {
        try {
            ConfigReader.readConfig("noSuchFile.xml");
        } catch (ValidatorInitException ex) {
            assertEquals("Configuration file does not exist.", ex.getMessage());

        }
    }

    @Test
    public void test_class_not_found() {
        try {
            ConfigReader.readConfig(pathToInvalidClassConfig);
        } catch (ValidatorInitException ex) {
            assertEquals("Configured check class could not be resolved", ex.getMessage());
        }
    }

    @Test
    public void test_invalid_type() {
        try {
            ConfigReader.readConfig(pathToInvalidTypeConfig);
        } catch (ValidatorInitException ex) {
            assertEquals("configured class is of wrong type", ex.getMessage());
        }
    }

    @Test
    public void test_wrong_configuration() {
        try {
            ConfigReader.readConfig(pathToInvalidFormatConfig);
        } catch (ValidatorInitException ex) {
            assertEquals("The XML-File seems to be invalid", ex.getMessage());
        }
    }

    @Test
    public void test_invalid_file() {
        try {
            ConfigReader.readConfig(pathToInvalidXMLConfig);
        } catch (ValidatorInitException ex) {
            assertEquals("Error parsing config-file.", ex.getMessage());
        }
    }

    @Test
    public void test_config_reader() throws Exception {
        Configuration config = ConfigReader.readConfig(pathToConfigFile);
        assertEquals("default", config.getQuarantineDir());
        Check check = config.getChecks().get(0);
        assertEquals("org.evolvis.fupco.checks.NullCheck", check.getClass().getName());
        assertNotNull(check.getProperty("test"));
        assertEquals("wert", check.getProperty("test"));
    }
}
