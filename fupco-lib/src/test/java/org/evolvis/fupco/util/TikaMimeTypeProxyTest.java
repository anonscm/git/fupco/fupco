/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.util;

import org.evolvis.fupco.checks.util.TikaMimeTypeProxy;
import junit.framework.Assert;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MimeTypes;
import org.junit.Test;

import java.util.List;

public class TikaMimeTypeProxyTest {
	private MimeTypes types = TikaConfig.getDefaultConfig().getMimeRepository();

	@Test
	public void deserializationTest() throws Exception{
		TikaMimeTypeProxy proxy = new TikaMimeTypeProxy(types.forName("image/jpeg"));
		List<String> knownExtensions = proxy.getExtensions();
		Assert.assertTrue(knownExtensions.size() > 1);
		Assert.assertTrue(knownExtensions.contains(".jpg"));
		Assert.assertTrue(knownExtensions.contains(".jpeg"));
	}
}
