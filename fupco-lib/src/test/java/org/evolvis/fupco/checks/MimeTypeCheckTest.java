/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * a test class for the {@link MimeTypeCheck}
 *
 * @author hgross
 */
public class MimeTypeCheckTest {

    private final static String pathToPDF = "/MimeTypeCheckPDF";
    private final static String pathToJPG = "/MimeTypeCheckJPG";
    private final static String pathToGIF = "/MimeTypeCheckPNG";
    private final static String pathToExe = "/wait.exe";
    private final static String pathToRtf = "/test.rtf";
    private final static String pathToODTwithWrongExtension = "/MimeTypeCheckODT.doc";
    private final static String MIME_TYPE_STRING_SEPARATOR = "mimeTypeStringSeparator";
    private final static String MIME_TYPES_ALLOWED = "allowedMimeTypes";
    private MimeTypeCheck mimeChecker;

    @Before
    public void init() {
        mimeChecker = new MimeTypeCheck().withProperty(MIME_TYPE_STRING_SEPARATOR, ",");
    }

    @Test
    public void testDoCheckWithoutMimeTypesSet() {
        File fileToCheck = new File(pathToGIF);
        assertFalse(mimeChecker.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_with_bogus_mime_type() {
        File fileToCheck = new File(pathToGIF);
        mimeChecker.withProperty(MIME_TYPES_ALLOWED, "application/bogus");
        CheckResult result = mimeChecker.doCheck(fileToCheck);
        assertFalse(result.isCheckSuccessful());
    }

    @Test
    public void testDoCheckWithNonExistingFile() {
        File fileToCheck = new File("NoSuchFile");
        mimeChecker.withProperty(
                MIME_TYPES_ALLOWED, "*");
        CheckResult result = mimeChecker.doCheck(fileToCheck);
        assertFalse(result.isCheckSuccessful());
    }

    @Test
    public void testDoCheckFilesWithoutExtension() throws URISyntaxException {
        String separator = ",";
        mimeChecker.withProperty(
                MIME_TYPES_ALLOWED,
                "application/pdf" + separator + "image/jpeg");

        File pdf = new File(getClass().getResource(pathToPDF).toURI());
        assertTrue(mimeChecker.doCheck(pdf).isCheckSuccessful());

        File jpg = new File(getClass().getResource(pathToJPG).toURI());
        assertTrue(mimeChecker.doCheck(jpg).isCheckSuccessful());

        File gif = new File(getClass().getResource(pathToGIF).toURI());
        assertFalse(mimeChecker.doCheck(gif).isCheckSuccessful());
    }

    @Test
    public void testDoCheckInvalidExtension() throws URISyntaxException {
        mimeChecker.withProperty(MIME_TYPES_ALLOWED,
                "application/vnd.oasis.opendocument.text");
        File invalidExtension = new File(getClass().getResource(pathToODTwithWrongExtension).toURI());
        CheckResult result = mimeChecker.doCheck(invalidExtension);
        assertTrue(result.isCheckSuccessful());
    }

    @Test
    public void testDoCheckRTF() throws URISyntaxException {
        mimeChecker.withProperty(MIME_TYPES_ALLOWED, "application/rtf");
        File rtf = new File(getClass().getResource(pathToRtf).toURI());
        CheckResult result = mimeChecker.doCheck(rtf);
        assertTrue(result.isCheckSuccessful());
    }

    @Test
    public void testDoCheckExe() throws URISyntaxException {
        mimeChecker.withProperty(MIME_TYPES_ALLOWED, "application/x-msdownload");
        File exe = new File(getClass().getResource(pathToExe).toURI());
        CheckResult result = mimeChecker.doCheck(exe);
        //System.out.println("Detected mime-type: " + result.getDetectedValue());

        assertTrue(result.isCheckSuccessful());

    }
}
