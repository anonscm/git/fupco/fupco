/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URISyntaxException;

import static org.junit.Assert.assertTrue;

/**
 * This test verifies that the bug #6304 is fixed and stays fixed.
 */

public class Bug6305 {

    private final String pathToDoc = "/mimeTypeTest.docx";
    private final String pathToXls = "/mimeTypeTest.xlsx";
    private final String pathToPpt = "/mimeTypeTest.pptx";

    private final static String MIME_TYPE_STRING_SEPARATOR = "mimeTypeStringSeparator";
    private final static String MIME_TYPES_ALLOWED = "allowedMimeTypes";
    private MimeTypeCheck mimeTypeCheck;
    private final  Logger log = LoggerFactory.getLogger(getClass());

    @Before
    public void initialize() {
        mimeTypeCheck = new MimeTypeCheck()
                .withProperty(MIME_TYPE_STRING_SEPARATOR, ",")
                .withProperty(MIME_TYPES_ALLOWED, "application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.openxmlformats-officedocument.presentationml.presentation");
    }

    @Test
    public void test_doc_upload() throws URISyntaxException {
        File fileToCheck =   new File(getClass().getResource(pathToDoc).toURI());
        CheckResult result = mimeTypeCheck.doCheck(fileToCheck);
        log.info("Detected: [{}]", result.getDetectedValue());
         assertTrue(result.isCheckSuccessful());
    }

    @Test
    public void test_xls_upload() throws URISyntaxException {
        File fileToCheck =   new File(getClass().getResource(pathToXls).toURI());
        CheckResult result = mimeTypeCheck.doCheck(fileToCheck);
         log.info("Detected: [{}]", result.getDetectedValue());
         assertTrue(result.isCheckSuccessful());
    }


    @Test
    public void test_ppt_upload() throws URISyntaxException {
        File fileToCheck =   new File(getClass().getResource(pathToPpt).toURI());
        CheckResult result = mimeTypeCheck.doCheck(fileToCheck);
        log.info("Detected: [{}]", result.getDetectedValue());
         assertTrue(result.isCheckSuccessful());
    }
}
