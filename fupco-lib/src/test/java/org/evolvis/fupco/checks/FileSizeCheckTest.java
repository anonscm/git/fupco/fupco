/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.evolvis.fupco.checks;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * A test class for the {@link FileSizeCheck}
 *
 * @author Florian Wallner
 */
public class FileSizeCheckTest {

    private FileSizeCheck sizeCheck;
    private File fileToCheck;
    private File emptyFile;
    private final static String MAX_FILE_SIZE = FileSizeCheck.MAXFILESIZE;
    private String path = "/FileSizeVerifierTest.png";
    private String emptyPath = "/emptyFile.txt";

    @Before
    public void setUp() throws URISyntaxException {
        this.sizeCheck = new FileSizeCheck();
        this.fileToCheck = new File(this.getClass().getResource(path).toURI());
        this.emptyFile = new File(this.getClass().getResource(emptyPath).toURI());

    }

    @Test
    public void test_empty_size_property() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "");
        assertFalse(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_empty_file() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1048576");
        CheckResult result = this.sizeCheck.doCheck(emptyFile);
        System.out.println("Discovered Size: " + result.getDetectedValue());
        System.out.println("Message: " + result.getMessage());
        assertFalse(result.isCheckSuccessful());
    }


    @Test
    public void test_empty_forbidden() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1048576");
        CheckResult result = this.sizeCheck.doCheck(emptyFile);
        System.out.println("Message: " + result.getMessage());
        assertEquals("check.filesize.empty.failure", result.getMessage());
        assertFalse(result.isCheckSuccessful());
    }

    @Test
    public void test_empty_allowed() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1048576").withProperty(FileSizeCheck.EMPTYFILE_ALLOWED, "true");
        CheckResult result = this.sizeCheck.doCheck(emptyFile);
        assertTrue(result.isCheckSuccessful());
    }

    @Test
    public void test_success_size() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1048576");
        assertTrue(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_success_size_k() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "350k");
        assertTrue(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_success_size_m() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1m");
        assertTrue(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_success_size_kb() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "350kb");
        assertTrue(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_success_size_mb() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1mb");
        assertTrue(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_file_size_too_large() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1024");
        assertFalse(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_file_size_too_large_k() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1k");
        assertFalse(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_file_size_too_large_kb() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "1kb");
        assertFalse(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_property_contains_invalid_characters() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "102sdfsd4");
        assertFalse(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }

    @Test
    public void test_property_is_larger_than_long() {
        this.sizeCheck.withProperty(MAX_FILE_SIZE, "-53423546234527845275426852485662567567468534563478563478563784567834657865783465786378465783567836578365783465783465378658658344527");
        assertFalse(this.sizeCheck.doCheck(fileToCheck).isCheckSuccessful());
    }
}
