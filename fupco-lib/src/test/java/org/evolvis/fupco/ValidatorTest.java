/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco;

import org.evolvis.fupco.checks.FailCheck;
import org.evolvis.fupco.checks.NullCheck;
import org.evolvis.fupco.config.Configuration;
import org.evolvis.fupco.exceptions.ValidatorValidationException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * A test class for the {@link Validator}
 *
 * @author Florian Wallner
 */
public class ValidatorTest {

    private Validator validator;

    @Before
    public void setUp() {
        validator = new Validator();
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test
    public void test_new_Validate_with_File() throws Exception {
        NullCheck nullVerifier = new NullCheck();
        Configuration config = new Configuration().addVerifyingCheck(nullVerifier);
        File inputFile = new File(getClass().getResource("/shortcuts.pdf").toURI());
        UploadedFile result = validator.validate(inputFile, config);
        assertFalse(result.isModified());
        assertEquals(nullVerifier, result.getCheckForClass(NullCheck.class));
        assertEquals(inputFile, result.getUploadedFile());
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test
    public void test_new_Validate_with_InputStream() throws Exception {
        NullCheck nullVerifier = new NullCheck();
        Configuration config = new Configuration().addVerifyingCheck(nullVerifier);
        InputStream stream = getClass().getResourceAsStream("/shortcuts.pdf");
        UploadedFile result = validator.validate(stream, config);
        assertFalse(result.isModified());
        assertEquals(nullVerifier, result.getCheckForClass(NullCheck.class));
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test
    public void test_old_Validate_with_File() throws Exception {
        NullCheck nullVerifier = new NullCheck();
        Configuration config = new Configuration().addVerifyingCheck(nullVerifier);
        File inputFile = new File(getClass().getResource("/shortcuts.pdf").toURI());
        UploadedFile result = validator.validate(config, inputFile);
        assertFalse(result.isModified());
        assertEquals(nullVerifier, result.getCheckForClass(NullCheck.class));
        assertEquals(inputFile, result.getUploadedFile());
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test
    public void test_validate_with_multiple_checks() throws Exception {
        NullCheck nullVerifier = new NullCheck();
        Configuration config = new Configuration().addVerifyingCheck(nullVerifier).addVerifyingCheck(new FailCheck());
        File inputFile = new File(getClass().getResource("/shortcuts.pdf").toURI());
        UploadedFile result = validator.validate(inputFile, config);
        assertFalse(result.isModified());
        assertEquals(nullVerifier, result.getCheckForClass(NullCheck.class));
        assertEquals(2, result.getExecutedChecks().size());
        assertFalse(result.isValidationSuccess());
        //assertEquals("fail verified", result.getFailedCheck().getCheckResult().getMessage());
        assertEquals("check.failcheck.value", result.getFailedCheck().getCheckResult().getDetectedValue());
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test
    public void test_old_Validate_with_InputStream() throws Exception {
        NullCheck nullVerifier = new NullCheck();
        Configuration config = new Configuration().addVerifyingCheck(nullVerifier);
        InputStream stream = getClass().getResourceAsStream("/shortcuts.pdf");
        UploadedFile result = validator.validate(config, stream);
        assertFalse(result.isModified());
        assertEquals(nullVerifier, result.getCheckForClass(NullCheck.class));
    }

    /**
     * Test of validate method, of class Validator.
     */
    public void test_new_validation_fail_with_File() throws Exception {
        FailCheck failVerifier = new FailCheck();
        Configuration config = new Configuration().addVerifyingCheck(failVerifier);
        File inputFile = new File(getClass().getResource("/shortcuts.pdf").toURI());
        UploadedFile result = validator.validate(inputFile, config);
        assertFalse(result.isValidationSuccess());
        assertEquals(failVerifier, result.getFailedCheck());
        assertEquals(inputFile, result.getUploadedFile());
    }

    /**
     * Test of validate method, of class Validator.
     */
    public void test_validation_validation_fail_with_InputStream() throws Exception {
        FailCheck failVerifier = new FailCheck();
        Configuration config = new Configuration().addVerifyingCheck(failVerifier);
        InputStream stream = getClass().getResourceAsStream("/shortcuts.pdf");
        UploadedFile result = validator.validate(stream, config);
        assertFalse(result.isValidationSuccess());
        assertEquals(failVerifier, result.getFailedCheck());
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test(expected = ValidatorValidationException.class)
    public void test_old_validation_fail_with_File() throws Exception {
        FailCheck failVerifier = new FailCheck();
        Configuration config = new Configuration().addVerifyingCheck(failVerifier);
        File inputFile = new File(getClass().getResource("/shortcuts.pdf").toURI());
        validator.validate(config, inputFile);
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test(expected = ValidatorValidationException.class)
    public void test_old_validation_fail_with_InputStream() throws Exception {
        FailCheck failVerifier = new FailCheck();
        Configuration config = new Configuration().addVerifyingCheck(failVerifier);
        InputStream stream = getClass().getResourceAsStream("/shortcuts.pdf");
        validator.validate(config, stream);
    }

    /**
     * Test of validate method, of class Validator.
     */
    @Test
    public void test_old_validation_fail_with_ResultFile_set() throws URISyntaxException {
        FailCheck failVerifier = new FailCheck();
        Configuration config = new Configuration().addVerifyingCheck(failVerifier);
        File inputFile = new File(getClass().getResource("/shortcuts.pdf").toURI());


        try {
            validator.validate(config, inputFile);
        } catch (ValidatorValidationException ex) {
            // left empty on purpose
        }
        assertEquals(inputFile.length(), validator.getFileUnderTest().length());
    }
}
