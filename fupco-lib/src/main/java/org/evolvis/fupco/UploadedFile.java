/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco;

import org.evolvis.fupco.checks.Check;
import org.evolvis.fupco.checks.CheckResult;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The result-object to return by the {@link Validator} it contains the executed {@link Check}s
 * ({@link CheckResult} can be accessed through the {@link Check})and the uploaded {@link File}
 * along with a flag (<code>modified</code>)
 * representing if the <code>file</code> has been modified during the check-phase or not
 *
 * @author Florian Wallner
 */
public class UploadedFile {

    /** The checks that were executed on the uploaded File */
    private List<Check> executedChecks = new ArrayList<Check>();
    /** The file uploaded by the user **/
    private File uploadedFile;
    /** whether the file was modified while processing or not. */
    private boolean modified = false;
    /** indicate wether the validation was successful or not */
    private boolean validationSuccess = false;

    /** The check that failed during validation */
    private Check failedCheck = null;

    /**
     * Provide an unmodifiable <code>List</code> of the <link>Check</link>
     * executed on the user provided file.
     *
     * @return An unmodifiable <code>List</code> of the <link>Check</link>s
     *         executed on the file.
     */
    public List<Check> getExecutedChecks() {
        return Collections.unmodifiableList(executedChecks);
    }

    /**
     * Provide access to the check of the given class executed on this file. It
     * takes a <code>Class</code> of the <code>Check</code> that the result is
     * wanted for. If no <code>Check</code> of the given <code>Class</code> was
     * run on the file <code>null</code> is returned.
     * @param classz A <code>Class</code> of a Check that should have been
     * executed on the file
     * @return The <code>Check</check> of the given class or <code>null</code>
     * if no such check was executed.
     */
    public Check getCheckForClass(Class<? extends Check> classz) {
        for (Check check : executedChecks) {
            if (check.getClass().equals(classz)) {
                return check;
            }
        }
        return null;
    }

    /**
     * Provide access to the test that failed the validation. This method returns
     * <code>null</code> if the validation was successful.
     * @return The Check that failed the validation. <code>null</code> if
     * all checks were successful.
     */
    public Check getFailedCheck(){
        return failedCheck;
    }

    /**
     * Set the Check that failed the validation. The default (package wide)
     * visibility is intentional.
     * @param failedCheck The {@link Check} that failed the validation.
     */
    void setFailedCheck(Check failedCheck) {
        this.failedCheck = failedCheck;
    }

    /**
     * Add the successfully executed check to the set of checks executed on the
     * uploaded file. This method uses the default (package wide) visibility
     * because it is not intended for public use.
     *
     * @param executedCheck
     *            The <code>Check</code> successfully executed on this File.
     */
    void addExecutedChecks(Check executedCheck) {
        this.executedChecks.add(executedCheck);
    }

    /**
     * Indicates whether the file was modified in the process of validation.
     *
     * @return <code>true</code> if the file was modified during upload.
     *         <code>false</code> otherwise.
     */
    public boolean isModified() {
        return modified;
    }

    /**
     * Indicate that the uploaded file was modified in the process of uploading it.
     *
     * @param modified <code>true</code> if the file was modified while verifying it.
     */
    public void setModified(boolean modified) {
        this.modified = modified;
    }

    /**
     * Returns the file uploaded by the user.
     * @return The file uploaded by the user.
     */
    public File getUploadedFile() {
        return uploadedFile;
    }

    /**
     * Returns <code>true</code> if all checks completed successfully
     * on the uploaded File, <code>false</code> otherwise.
     * @return  <code>true</code> if the validation was successful, <code>false</code>
     */
    public boolean isValidationSuccess() {
        return validationSuccess;
    }

    /**
     * Set the success of the validation. The package wide visibility is
     * intentional. End users are not supposed to change the outcome of a validation.
     * @param success the outcome of the validation.
     */
    void setValidationSuccess(boolean success) {
        this.validationSuccess = success;
    }

    /**
     * Set the uploaded file. It intentionally uses package wide visibility.
     * @param uploadedFile the file uploaded by the user
     */
    void setUploadedFile(File uploadedFile) {
        this.uploadedFile = uploadedFile;
    }
}
