/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco;

import org.evolvis.fupco.checks.Check;
import org.evolvis.fupco.checks.CheckResult;
import org.evolvis.fupco.checks.ModifyingCheck;
import org.evolvis.fupco.config.Configuration;
import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.evolvis.fupco.exceptions.ValidatorValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * A {@link Validator}-instance performs {@link Check}s configured in a given
 * {@link Configuration} instance on a given {@link File}/{@link InputStream}.
 *
 * @author Florian Wallner
 */
public class Validator {

    private File fileUnderTest;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public UploadedFile validate(File file, Configuration config) {
        fileUnderTest = file;
        UploadedFile result = new UploadedFile();

        result.setUploadedFile(file);

        for (Check check : config.getChecks()) {
            result.addExecutedChecks(check);
            CheckResult checkResult = check.doCheck(file);

            if (check instanceof ModifyingCheck && ((ModifyingCheck) check).isModified()) {
                result.setModified(true);
            }

            if (!checkResult.isCheckSuccessful()) {
                result.setFailedCheck(check);
                logger.error("File validation failed. message is: " + checkResult.getMessage());
                return result;
            }


        }
        logger.info("File validation successful.");
        result.setValidationSuccess(true);
        return result;
    }

    /**
     * performs {@link Check}s configured in given {@link Configuration}
     * instance on a given {@link File}
     * @param config
     * @param file
     * @return
     * @throws ValidatorValidationException
     */
    @Deprecated
    public UploadedFile validate(Configuration config, File file)
            throws ValidatorValidationException {
        UploadedFile result = validate(file, config);
        if (!result.isValidationSuccess()) {
            throw new ValidatorValidationException(result.getFailedCheck());
        }
        return result;
    }

    /**
     * performs {@link Check}s configured in given {@link Configuration}
     * instance on a given {@link InputStream}
     * @param config
     * @param stream
     * @return
     * @throws ValidatorInitException
     */
    public UploadedFile validate(InputStream stream, Configuration config) throws ValidatorInitException {

        File tmpFile = createTmpFile(config);
        tmpFile = storeFile(stream, tmpFile);
        return validate(tmpFile, config);
    }

    /**
     * performs {@link Check}s configured in given {@link Configuration}
     * instance on a given {@link InputStream}
     * @param config
     * @param stream
     * @return
     * @throws ValidatorInitException
     * @throws ValidatorValidationException
     */
    @Deprecated
    public UploadedFile validate(Configuration config, InputStream stream) throws ValidatorInitException, ValidatorValidationException {
        UploadedFile result = validate(stream, config);
        if (!result.isValidationSuccess()) {
            throw new ValidatorValidationException(result.getFailedCheck());
        }
        return result;
    }

    /**
     *  provide access to the File under test by this Validator.
     * @return the File under Test
     */
    public File getFileUnderTest() {
        return fileUnderTest;
    }

    /**
     * helper method to create a tmp file
     * @param config
     * @return
     * @throws ValidatorInitException
     */
    private File createTmpFile(Configuration config) throws ValidatorInitException {

        String fileName = java.util.UUID.randomUUID().toString();
        File tmpFile;
        try {
            tmpFile = File.createTempFile(fileName, ".fupco.tmp", new File(config.getQuarantineDir()));
        } catch (IOException ex) {
            throw new ValidatorInitException(ex);
        }
        tmpFile.deleteOnExit();
        return tmpFile;
    }

    /**
     * helper method to store a file
     * @param is
     * @param file
     * @return
     * @throws IOException
     */
    private File storeFile(InputStream is, File file) throws ValidatorInitException {

        //write stream to file
        OutputStream out;
        try {
            out = new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            throw new ValidatorInitException("Can't store File for testing", ex);
        }
        byte buf[] = new byte[4096];
        int len;
        try {
            while ((len = is.read(buf)) > 0) {
                out.write(buf, 0, len);
                out.flush(); // does nothing?
            }
        } catch (IOException ex) {
            throw new ValidatorInitException("Can't store file content for validation", ex);
        } finally {
            try {
                out.close();
                is.close();
            } catch (IOException ex) {
                // so what?
            }
        }
        return file;
    }
    //close streams
}
