/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.config;

import org.evolvis.fupco.checks.Check;
import org.evolvis.fupco.checks.ModifyingCheck;
import org.evolvis.fupco.checks.VerifyingCheck;
import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * {@link ConfigReader} is a utility Class to deserialize a Fupco XML-Configuration file into a {@link Configuration}
 * object.
 *
 * @author Henning Gross
 * @author Florian Wallner
 */
public class ConfigReader {

    /**
     * This constructor does nothing and is intentionally kept in private scope because this class only has
     * static methods. So it should be impossible to instantiate it.
     */
    private ConfigReader() {

    }

    /**
     * <code>readConfig</code> returns a {@link Configuration} - instance
     * deserialized from a XML configuration file.
     *
     * @param pathToFile The path to the configuration file.
     * @return The {@link Configuration} object configured as specified in the configuration file.
     * @throws ValidatorInitException If it's not possible to create the {@link Configuration} as specified in the
     *                                configuration File
     */
    public static Configuration readConfig(String pathToFile)
            throws ValidatorInitException {
        Configuration config = new Configuration();
        File file = new File(pathToFile);
        if (!file.exists()) {
            throw new ValidatorInitException(
                    "Configuration file does not exist.");
        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        Document doc;

        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
        } catch (Exception e) {
            throw new ValidatorInitException("Error parsing config-file.", e);
        }

        // is the root element named "configuration" ?
        if (!"configuration".equals(doc.getDocumentElement().getNodeName())) {
            throw new ValidatorInitException("The XML-File seems to be invalid");
        }

        // handle quarantineDir-element
        NodeList qDir = doc.getElementsByTagName("quarantineDir");
        if (qDir.getLength() == 1) {
            config.setQuarantineDir(qDir.item(0).getTextContent());
        }

        // handle checks
        NodeList checks = doc.getElementsByTagName("check");

        for (int i = 0; i < checks.getLength(); i++) {
            try {
                Check check = handleCheckNode(checks.item(i));

                if (check instanceof VerifyingCheck) {
                    config.addVerifyingCheck((VerifyingCheck) check);
                } else {
                    config.addModifyingCheck((ModifyingCheck) check);
                }

            } catch (ClassNotFoundException e) {
                throw new ValidatorInitException(
                        "Configured check class could not be resolved", e);
            } catch (InstantiationException e) {
                throw new ValidatorInitException(
                        "Configured check class could not be instantiated", e);
            } catch (IllegalAccessException e) {
                throw new ValidatorInitException(
                        "Configured check class could not be instantiated", e);
            }
        }

        return config;
    }

    /**
     * helper method to deserialize a Check object from a XML fragment.
     *
     * @param checkNode The XML-Fragment representing the Check
     * @return A deserialized {@link Check}
     * @throws ClassNotFoundException If the specified class for the Check can not be found.
     * @throws InstantiationException If the instantiation of the specified class did not succeed.
     * @throws IllegalAccessException Sometimes.
     * @throws ValidatorInitException If the configuration of the Check according to the specified values did not succeed.
     */
    private static Check handleCheckNode(Node checkNode) throws ClassNotFoundException, InstantiationException, IllegalAccessException, ValidatorInitException {
        String checkName = checkNode.getAttributes().item(0).getNodeValue();
        Class<?> checkClass = Class.forName(checkName);
        Object oCheck = checkClass.newInstance();

        if (!(oCheck instanceof Check)) {
            throw new ValidatorInitException(
                    "configured class is of wrong type");
        }

        Check check = (Check) oCheck;

        NodeList nlProperties = ((Element) checkNode)
                .getElementsByTagName("property");

        for (int j = 0; j < nlProperties.getLength(); j++) {
            NamedNodeMap propertySet = nlProperties.item(j).getAttributes();
            String key = propertySet.item(0).getNodeValue();
            String value = propertySet.item(1).getNodeValue();
            check.withProperty(key, value);
        }
        return check;
    }
}
