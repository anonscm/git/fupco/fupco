/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.config;

import org.evolvis.fupco.checks.Check;
import org.evolvis.fupco.checks.ModifyingCheck;
import org.evolvis.fupco.checks.VerifyingCheck;
import org.evolvis.fupco.exceptions.ValidatorInitException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * This class provides the configuration necessary to perform an upload
 * validation on uploaded File.
 *
 * @author Florian Wallner
 */
public class Configuration{
	private String quarantineDir = System.getProperty("java.io.tmpdir");

	private List<Check> checks = new ArrayList<Check>();

	public static Configuration readConfig(String pathToFile)
			throws ValidatorInitException {
		return ConfigReader.readConfig(pathToFile);
	}

	public List<Check> getChecks() {
		return Collections.unmodifiableList(checks);
	}

	public Configuration addModifyingCheck(ModifyingCheck check) {
		checks.add(check);
		return this;
	}

	public Configuration addVerifyingCheck(VerifyingCheck check) {
		checks.add(check);
		return this;
	}

	/**
	 * Convenience method to set the quarantine directory of this configuration
	 * Object. If none is set it defaults to the directory specified in
	 * "java.io.tmpdir"
	 *
	 * @param path
	 *            The path to use for quarantining incoming files in.
	 * @return itself with the quarantine directory set to the new string.
	 */
	public Configuration withQuarantineDir(String path) {
		this.quarantineDir = path;
		return this;
	}

	public String getQuarantineDir() {
		return quarantineDir;
	}

	public void setQuarantineDir(String quarantineDir) {
		this.quarantineDir = quarantineDir;
	}
}
