/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks.util;

import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.apache.tika.config.TikaConfig;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MimeType {
	private String mimeType;
	private List<String> extensions = new ArrayList<String>();

	public MimeType(MediaType mediaType) throws MimeTypeException, ValidatorInitException {
        this(mediaType.toString());
	}

    public MimeType(String mimeType) throws MimeTypeException, ValidatorInitException {
    	MimeTypes tikaMimeTypes = TikaConfig.getDefaultConfig().getMimeRepository();
		this.mimeType = mimeType;
		TikaMimeTypeProxy proxiedTikaMimeType = new TikaMimeTypeProxy(tikaMimeTypes.forName(mimeType));
		this.extensions = proxiedTikaMimeType.getExtensions();
    }

	/**
	 * returns a string representing the Mime Type. A well known example would be "text/plain" for plain text Documents.
	 * @return a String representing this Mime Type.
	 */
	public String getMimeType(){
		return this.mimeType;
	}

	/**
	 * returns a List
	 * @return {@link java.util.Collections.UnmodifiableList} with the known valid extensions
	 */
	public List<String> getExtensions(){
		return Collections.unmodifiableList(this.extensions);
	}
}
