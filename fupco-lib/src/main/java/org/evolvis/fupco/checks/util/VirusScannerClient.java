/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks.util;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Florian Wallner
 */
public class VirusScannerClient {

    /** The logger */
 //   private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final int virusScannerPort;
    private final String virusScannerHost;
    private final String[] acceptableReturns;

    public VirusScannerClient(String virusScannerHost, String virusScannerPort, String acceptableReturnValues) {

        this.virusScannerHost = virusScannerHost;
        this.virusScannerPort = Integer.parseInt(virusScannerPort);
        this.acceptableReturns = acceptableReturnValues.split("\\s*,\\s*");
        if (acceptableReturns.length < 1) {
            throw new IllegalArgumentException("No acceptable states for virus scan defined");
        }

    }

    /**
     * Scan the given file using the F-Prot daemon
     * @param file The file to scan
     * @return the result string returned by F-Prot
     * @throws IOException
     * @throws UnknownHostException
     */
    private String avScanFile(File file) throws UnknownHostException, IOException {
        Socket fprot = new Socket(virusScannerHost, virusScannerPort);
        String commandFormatString = "SCAN FILE %1$s\n";
        String command = String.format(commandFormatString, file.getAbsoluteFile());

        fprot.getOutputStream().write(command.getBytes());

        StringBuilder result = new StringBuilder();
        int x;
        while ((x = fprot.getInputStream().read()) != '\n') {
            result.append((char) x);
        }
   //     logger.info("Scanned File " + file.getAbsoluteFile() + " Result: " + result);
        return result.toString();
    }

    /**
     * Perform a virus scan on the given file
     * @param file The file to scan for viruses
     * @return An {@link ScanResult} object representing the result of the scan
     */
    public ScanResult scanFile(File file) throws VirusScannerException {

        String result;

        try {
            result = avScanFile(file);
        } catch (IOException ex) {
            throw new VirusScannerException("unable to communicate with F-prot", ex);
        }
        Pattern p = Pattern.compile("^\\d+");
        Matcher m = p.matcher(result);
        if (!m.find()) {
            throw new VirusScannerException("Can't find return code in F-Prot result: " + result);
        }
        String resultCode = m.group();
        if (resultCode != null && !"".equals(resultCode)) {
            int returnCode = Integer.parseInt(resultCode);
            for (String acceptableReturn : acceptableReturns) {
                if (returnCode == Integer.parseInt(acceptableReturn)) {
                    return new ScanResult(file, true);
                }
            }
        }
        return new ScanResult(file, false);
    }
}
