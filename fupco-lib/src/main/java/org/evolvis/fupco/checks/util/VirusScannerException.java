/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks.util;

/**
 *
 * @author Florian Wallner. tarent GmbH
 */
public class VirusScannerException extends Exception {

    /**
	 *
	 */
	private static final long serialVersionUID = -5293843212686430958L;

	public VirusScannerException(String message) {
        super(message);
    }

    public VirusScannerException(String message, Throwable cause) {
        super(message, cause);
    }

    public VirusScannerException(Throwable cause){
        super(cause);
    }
}
