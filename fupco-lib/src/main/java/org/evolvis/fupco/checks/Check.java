/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import java.io.File;
import java.util.Set;

/**
 * Every class that wants to check if an uploaded file is allowed into the
 * system must implement this interface or one of the interfaces extending it.
 *
 * @author Florian Wallner
 */
public interface Check {

    /**
     * perform the actual validation of the file.
     * @param fileToCheck The file this Check is supposed to run on.
     * @return <code>true</code> if the check was successful. <code>false</code> otherwise.
     */
    CheckResult doCheck(File fileToCheck);

    /**
     * A string describing the tests functionality. This method is intended for informational use.
     * @return A String describing the tests functionality.
     */
    String getCheckDescription();

    /**
     * Provide access to the configuration properties of this check.
     * @param propertyName The name of the configuration property to get the value for.
     * @return The value of the given configuration property.
     */
    String getProperty(String propertyName);

    /**
     * Provide a <code>Set</code> of all known configuration options of this check.
     * @return A <code>Set</code> of Strings with all required properties.
     */
    Set<String> getRequiredProperties();

    /**
     * This method provides access to all localizable Strings that this Check
     * knows about.
     *
     * @return All known localizable Strings.
     */
    Set<String> getAllMessageStrings();

    /**
     * Provides access to the checks result. If the check has not been run yet
     * this the {@link CheckResult} should inform the user about it.
     *
     * @return The result of this check.
     */
    CheckResult getCheckResult();

    /**
     * Takes a given configuration key and the corresponding value and sets this
     * configuration property to the given value. It returns itself to enable use
     * of the Builder pattern.
     *
     * @param key The name of the configuration key.
     * @param value The value for the given key.
     * @return The Check itself with the configuration value set.
     */
    Check withProperty(String key, String value);
}
