/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import java.io.File;

/**
 * Modifying {@link Check}s can modify the {@link File} e.g. a virus scan that
 * can disinfect a {@link File}
 *
 * @author Florian Wallner
 */
public interface ModifyingCheck extends Check {

    /**
     * Indicates whether this {@link Check} actually modified the uploaded file.
     *
     * @return <code>true</code> if the check modified the file.
     *         <code>false</code> otherwise.
     */
    boolean isModified();

    /**
     * builder-style setter for a configuration property
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    ModifyingCheck withProperty(String key, String value);
}
