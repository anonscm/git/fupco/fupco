/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import org.evolvis.fupco.checks.util.MimeType;
import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.apache.tika.Tika;
import org.apache.tika.mime.MimeTypeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class MimeTypeCheck extends AbstractCheck implements VerifyingCheck {

    private final static String UL_MIME_SUCCESS = "check.mimetype.result.success";
    private final static String UL_MIME_FAILURE = "check.mimetype.result.failure";
    private final static String UL_MIME_EXCEPTION = "check.mimetype.exception.failure";

    public final static String MIME_TYPE_STRING_SEPERATOR = "mimeTypeStringSeperator";
    public final static String MIME_TYPES_ALLOWED = "allowedMimeTypes";

    private Logger log = LoggerFactory.getLogger(MimeTypeCheck.class);

    private MimeType mimeType;

    public MimeTypeCheck() {
        requiredProperties.add(MIME_TYPES_ALLOWED);
        requiredProperties.add(MIME_TYPE_STRING_SEPERATOR);
        configurationProperties.put(MIME_TYPE_STRING_SEPERATOR, ",");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CheckResult doCheck(File fileToCheck) {

        if (!isInitialized(fileToCheck)) {
            return checkResult;
        }
        try {
            Tika tika = new Tika();

            this.mimeType = new MimeType(tika.detect(fileToCheck));

            log.debug("Detected Mime-Type is: [{}]", mimeType.getMimeType());
        } catch (MimeTypeException e) {
            log.error("Error detecting mime type of uploaded File: " + e.getMessage(), e);
            checkResult = CheckResult.FAIL(
                    UL_MIME_EXCEPTION, e);
            return checkResult;
        } catch (FileNotFoundException e) {
            // This exception is never thrown, File not found is handled in AbstractCheck
            checkResult = CheckResult.FAIL(
                    UL_MIME_EXCEPTION, e);
            return checkResult;
        } catch (IOException e) {
            checkResult = CheckResult.FAIL(
                    UL_MIME_EXCEPTION, e);
            return checkResult;
        } catch (ValidatorInitException e) {
            checkResult = CheckResult.FAIL(
                    UL_MIME_EXCEPTION, e);
            return checkResult;
        }

        String detectedMimeType = mimeType.getMimeType();
        List<String> allowedMimeTypes = Arrays.asList(getProperty(
                MIME_TYPES_ALLOWED).split(
                getProperty(MIME_TYPE_STRING_SEPERATOR)));

        if (allowedMimeTypes.contains(mimeType.getMimeType())) {
            checkResult = CheckResult.SUCCESS(UL_MIME_SUCCESS, detectedMimeType);
//            checkResult = CheckResult.SUCCESS("successfully checked determined mime type: "
//                    + detectedMimeType, detectedMimeType);
        } else {
            checkResult = CheckResult.FAIL(UL_MIME_FAILURE, detectedMimeType);
//            checkResult = CheckResult.FAIL("determined mime type: "
//                    + detectedMimeType + " is not allowed", detectedMimeType);
        }
        return checkResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCheckDescription() {
        return "a check that determines the mime-type of a file and matches it against a configured set of allowed types";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MimeTypeCheck withProperty(String key, String value) {
        super.withProperty(key, value);
        return this;
    }

    /**
     * Returns a {@link MimeType} Instance if the check was run and
     * a Mime type could be discovered, <code>null</code> otherwise.
     *
     * @return the {@link MimeType} of the uploaded file or <code>null</code>
     */
    public MimeType getDetectedMimeType() {
        return mimeType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getAllMessageStrings() {
        Set<String> resultSet = new HashSet<String>(3);

        resultSet.add(UL_MIME_FAILURE);
        resultSet.add(UL_MIME_SUCCESS);
        resultSet.add(UL_MIME_EXCEPTION);

        return Collections.unmodifiableSet(resultSet);
    }
}
