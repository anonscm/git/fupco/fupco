/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

/**
 * {@link CheckResult} is the Object to return by a {@link Check}. It contains a
 * {@link Boolean} representing if the {@link Check} was successful, a
 * {@link String} with a <code>message</code> and an optional field for a
 * {@link Throwable} that might have been thrown by an Exception that occurred while
 * the Check was executed.
 *
 * @author Henning Groß
 * @author Florian Wallner
 */
public class CheckResult {

    private boolean checkSuccessful = false;
    private String message = "check not run yet";
    private Throwable throwable;
    private String detectedValue = "";

    /**
     * Convenience factory method to initialize a successful {@link CheckResult} instance.
     * Use this method to initialize a {@link CheckResult} instance of a successfully run Check.
     * It takes only one parameter namely the message about the test run.
     *
     * @param message The message about the test run.
     * @return The initialized {@link CheckResult} object.
     */
    public static CheckResult SUCCESS(String message) {
        return new CheckResult().withCheckSuccessful(true).withMessage(message);
    }

    /**
     * Convenience factory method to initialize a successful {@link CheckResult} instance.
     * Use this method to initialize a {@link CheckResult} instance of a successfully run Check.
     *
     * @param message       The message about the test run.
     * @param detectedValue The value detected by the test.
     * @return The initialized {@link CheckResult} object.
     */
    public static CheckResult SUCCESS(String message, String detectedValue) {
        return new CheckResult().withCheckSuccessful(true).withMessage(message).withDetectedValue(detectedValue);
    }

    /**
     * Convenience factory method to initialize a failed {@link CheckResult} instance.
     * Use this method to initialize a {@link CheckResult} instance of a Check that failed.
     *
     * @param message       The message about the test run.
     * @param detectedValue The value detected by the test.
     * @return The initialized {@link CheckResult} object.
     */
    public static CheckResult FAIL(String message, String detectedValue) {
        return new CheckResult().withCheckSuccessful(false).withMessage(message).withDetectedValue(detectedValue);
    }

    /**
     * Convenience factory method to initialize a failed {@link CheckResult} instance.
     * Use this method to initialize a {@link CheckResult} instance of a Check that failed.
     *
     * @param message   The message about the test run.
     * @param throwable An exception that might have occurred while running the Check.
     * @return The initialized {@link CheckResult} object.
     */
    public static CheckResult FAIL(String message, Throwable throwable) {
        return new CheckResult().withCheckSuccessful(false).withMessage(message).withThrowable(throwable);
    }

    /**
     *  This method sets the <code>throwable</code> field and returns the CheckResult to enable
     *  the use of the builder pattern.
     *
     * @param throwable The throwable that occurred during the Check run.
     * @return The CheckRun Instance with the <code>throwable</code> set.
     */
    private CheckResult withThrowable(Throwable throwable) {
        this.throwable = throwable;
        return this;
    }

    /**
     *  This method sets the <code>message</code> field and returns the CheckResult to enable
     *  the use of the builder pattern.
     *
     * @param message The that was produced during the Check run.
     * @return The CheckRun Instance with the <code>message</code> set.
     */

    private CheckResult withMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     *  Sets the <code>checkSuccessful</code> field to indicate if the Check completed successfully
     *  or failed. It returns the CheckResult to enable the use of the builder pattern.
     *
     * @param checkSuccessful The result of the Chek run.
     * @return The CheckRun Instance with the <code>checkSuccessful</code> set to <code>true</code> or <code>false</code>.
     */
    private CheckResult withCheckSuccessful(boolean checkSuccessful) {
        this.checkSuccessful = checkSuccessful;
        return this;
    }

    /**
     *  Sets the <code>detectedValue</code> field and returns the CheckResult to enable
     *  the use of the builder pattern.
     *
     * @param detectedValue The that was produced during the Check run.
     * @return The CheckRun Instance with the <code>detectedValue</code> set.
     */
    private CheckResult withDetectedValue(String detectedValue) {
        this.detectedValue = detectedValue;
        return this;
    }

    /**
     * Returns the {@link Throwable} that was produced while the Check was running, Returns <code>null</code> if no
     * {@link Exception} occurred during the run.
     *
     * @return The {@link Throwable} that occured during the Check or <code>null</code> if none happened.
     */
    public Throwable getThrowable() {
        return throwable;
    }

    /**
     * returns the result string <code>message</code> or
     * <code>"check not run yet"</code>
     *
     * @return the result String generated by the Check or the String <code>"check not run yet"</code>
     */
    public String getMessage() {
        return message;
    }

    /**
     * Returns <code>true</code> if the {@link Check} was run successful or
     * <code>false</code> if it failed. I also returns <code>false</code> if the
     * {@link Check} has not been executed yet.
     *
     * @return The result of the Check run.
     */
    public boolean isCheckSuccessful() {
        return checkSuccessful;
    }

    /**
     * Returns the value detected by a check. for example the detected mime type
     * or the actual file size, returns an empty string if the check was not run
     *
     * @return The value detected by the Check or an empty string if the Check was not run.
     */
    public String getDetectedValue() {
        return detectedValue;
    }
}
