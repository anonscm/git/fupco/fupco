/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Check verifies that the given file does not exceed the given file size.
 *
 * @author Florian Wallner
 */
public class FileSizeCheck extends AbstractCheck implements VerifyingCheck {

    /**
     * The maximal allowed file size allowed for this Check to succeed.
     */
    public final static String MAXFILESIZE = "maxFileSize";
    /**
     * Wether empty files should be allowed or not. Defaults to 'false'.
     */
    public final static String EMPTYFILE_ALLOWED = "emptyFileAllowed";
    private final static String UL_SIZE_PROPERTY_FAIL = "check.filesize.init.failure";
    private final static String UL_SIZE_SUCCESS = "check.filesize.result.success";
    private final static String UL_SIZE_FAILURE = "check.filesize.result.failure";
    private final static String UL_SIZE_EMPTY_FAIL = "check.filesize.empty.failure";

    public FileSizeCheck() {
        requiredProperties.add(MAXFILESIZE);

    }

    @Override
    public CheckResult doCheck(File fileToCheck) {
        if (!isInitialized(fileToCheck)) {
            return checkResult;
        }
        long allowedFileSize = parseFileSize();
        boolean emptyFilesAllowed = Boolean.parseBoolean(configurationProperties.get(EMPTYFILE_ALLOWED));

        if (allowedFileSize == -1L) {
            checkResult = CheckResult.FAIL("Can't parse property " + configurationProperties.get(MAXFILESIZE), MAXFILESIZE);
        } else if (!emptyFilesAllowed && fileToCheck.length() == 0) {
            checkResult = CheckResult.FAIL(UL_SIZE_EMPTY_FAIL, Long.toString(fileToCheck.length()));
        } else if (fileToCheck.length() > allowedFileSize) {
            checkResult = CheckResult.FAIL(UL_SIZE_FAILURE, Long.toString(fileToCheck.length()));
        } else {
            checkResult = CheckResult.SUCCESS(UL_SIZE_SUCCESS, Long.toString(fileToCheck.length()));
        }
        return checkResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCheckDescription() {
        return "This Check verifies that the given file is smaller than the configured maximum.";
    }

    private long parseFileSize() {
        String configuredSize = configurationProperties.get(MAXFILESIZE);
        Pattern pattern = Pattern.compile("^(\\d+)([kmgt]b?)?$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(configuredSize);
        long allowedFileSize = -1L;

        if (matcher.matches()) {

            String digits = matcher.group(1);

            allowedFileSize = Long.parseLong(digits);
            if (matcher.group(2) != null) {
                String suffix = matcher.group(2).toLowerCase(Locale.ENGLISH);
                if (suffix.startsWith("k")) {
                    allowedFileSize = allowedFileSize * 1024;
                } else if (suffix.startsWith("m")) {
                    allowedFileSize = allowedFileSize * 1024 * 1024;
                } else if (suffix.startsWith("g")) {
                    allowedFileSize = allowedFileSize * 1024 * 1024 * 1024;
                } else if (suffix.startsWith("t")) {
                    allowedFileSize = allowedFileSize * 1024 * 1024 * 1024 * 1024;
                }
            }
        }
        return allowedFileSize;
    }

    @Override
    public FileSizeCheck withProperty(String key, String value) {
        super.withProperty(key, value);
        return this;
    }

    /**
     * {@inheritDoc }
     */
    @Override
    public Set<String> getAllMessageStrings() {
        Set<String> resultSet = new HashSet<String>(3);
        resultSet.add(UL_SIZE_FAILURE);
        resultSet.add(UL_SIZE_SUCCESS);
        resultSet.add(UL_SIZE_PROPERTY_FAIL);
        resultSet.add(UL_SIZE_EMPTY_FAIL);
        return Collections.unmodifiableSet(resultSet);
    }
}
