/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import java.io.File;

/**
 * a {@link VerifyingCheck} performs {@link Check} on a {@link File} without
 * modifying it
 *
 * @author Florian Wallner
 */
public interface VerifyingCheck extends Check {

    /**
     * builder-style setter for a configuration property
     *
     * @param key
     * @param value
     * @return
     */
    @Override
    VerifyingCheck withProperty(String key, String value);
}
