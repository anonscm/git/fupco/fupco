/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This is a very basic verifier that always returns true when it
 * @author Florian Wallner
 */
public class NullCheck extends AbstractCheck implements VerifyingCheck {

    private final static String UL_NULL_SUCCESS = "check.null.result.success";

    @Override
    public CheckResult doCheck(File fileToCheck) {
        this.checkResult = CheckResult.SUCCESS(UL_NULL_SUCCESS);
        return checkResult;
    }

    @Override
    public String getCheckDescription() {
        return "a simple check that always returns true";
    }

    @Override
    public NullCheck withProperty(String key, String value) {
        configurationProperties.put(key, value);
        return this;
    }

    @Override
    public Set<String> getAllMessageStrings() {
        Set<String> resultSet = new HashSet<String>(1);
        resultSet.add(UL_NULL_SUCCESS);
        return Collections.unmodifiableSet(resultSet);
    }
}
