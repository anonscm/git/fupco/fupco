/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import org.evolvis.fupco.checks.util.ScanResult;
import org.evolvis.fupco.checks.util.VirusScannerClient;
import org.evolvis.fupco.checks.util.VirusScannerException;

import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Florian Wallner
 */
public class VirusScanningCheck extends AbstractCheck implements ModifyingCheck {

    private final static String UL_VIRUS_SUCCESS = "check.virus.result.success";
    private final static String UL_VIRUS_FAILURE = "check.virus.result.failure";
    private final static String UL_VIRUS_EXCEPTION = "check.virus.exception.failure";


    public static final String SCANNER_HOST = "scannerHost";
    public static final String SCANNER_PORT = "scannerPort";
    public static final String ACCEPTABLE_RETURN_VALUES = "acceptableReturnValues";
    private boolean modified = false;
    private CheckResult result = new CheckResult();

    @Override
    public boolean isModified() {
        return modified;
    }

    @Override
    public VirusScanningCheck withProperty(String key, String value) {
        super.withProperty(key, value);
        return this;
    }

    @Override
    public CheckResult doCheck(File fileToCheck) {
        if (!isInitialized(fileToCheck)) {
            return result;
        }

        VirusScannerClient scanner = new VirusScannerClient(getProperty(SCANNER_HOST),
                getProperty(SCANNER_PORT),
                getProperty(ACCEPTABLE_RETURN_VALUES));

        ScanResult scanResult;
        try {
            scanResult = scanner.scanFile(fileToCheck);

        } catch (VirusScannerException ex) {
            result = CheckResult.FAIL("Error scanning file for viruses", ex);
            return result;
        }

        modified = scanResult.isModified();
        if (!scanResult.isClean()) {
            result = CheckResult.FAIL(UL_VIRUS_FAILURE, "infected");
        } else {
            result = CheckResult.SUCCESS(UL_VIRUS_SUCCESS, "clean");
        }
        return result;
    }

    @Override
    public String getCheckDescription() {
        return "This check scans the given file for viruses.";
    }

    @Override
    public Set<String> getAllMessageStrings() {
        Set<String> resultSet = new HashSet<String>(3);

        resultSet.add(UL_VIRUS_FAILURE);
        resultSet.add(UL_VIRUS_SUCCESS);
        resultSet.add(UL_VIRUS_EXCEPTION);

        return Collections.unmodifiableSet(resultSet);
    }
}
