/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks.util;

import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.apache.tika.mime.MimeType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * a proxy for the mime type object containing a workaround implementation of
 * getExtensions() in case of review, please check if tika 1.0 or later is
 * released this patch: https://issues.apache.org/jira/browse/TIKA-661 was
 * accepted and is already commited in tika trunk
 *
 * @author hgross
 */
public class TikaMimeTypeProxy {
    private static final String TIKAMIMECONFFILENAME = "tika-mimetypes.xml";
    private final MimeType mimeType;
    private List<String> extensions = new ArrayList<String>();

    public TikaMimeTypeProxy(MimeType mimeType) {
        this.mimeType = mimeType;
    }

    public List<String> getExtensions() throws ValidatorInitException {
        if (extensions.size() < 1) {
            deserializeTikaMimeTypes(mimeType.getName());
        }
        return extensions;
    }

    private void deserializeTikaMimeTypes(String mimeType)
            throws ValidatorInitException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        Document doc;

        try {
            InputStream stream = MimeType.class.getResource(
                    TIKAMIMECONFFILENAME).openStream();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(stream);
            doc.getDocumentElement().normalize();

        } catch (IOException e) {
            throw new ValidatorInitException("Error opening config-file.", e);
        } catch (ParserConfigurationException e) {
            throw new ValidatorInitException("Error parsing config-file.", e);
        } catch (SAXException e) {
            throw new ValidatorInitException("Error parsing config-file.", e);
        }

        NodeList mimeTypes = doc.getElementsByTagName("mime-type");
        for (int i = 0; i < mimeTypes.getLength(); i++) {
            Element mimeTypeNode = (Element) mimeTypes.item(i);
            String mimeString = mimeTypeNode.getAttribute("type");
            if (mimeType.equals(mimeString)) {
                NodeList globs = mimeTypeNode.getElementsByTagName("glob");
                for (int j = 0; j < globs.getLength(); j++) {
                    Node glob = globs.item(j);
                    this.extensions.add(((Element) glob)
                            .getAttribute("pattern").replace("*", ""));
                }
            }
        }
    }
}
