/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks.util;

import java.io.File;

/**
 * Objects of this class represent the result of anti virus scan.
 * @author Florian Wallner, tarent GmbH
 */
public class ScanResult {

    /** The File that was scanned.  */
    private final File file;
    /** is the file infected? */
    private final boolean clean;
    /** was this file modified while scanning it? */
    private final boolean modified;

    /**
     * Create a new <code>ScanResult</code> Object.
     * @param file The file the scan was performed on
     * @param clean <code>true</code> if the file was clean, <code>false</code> otherwise.
     * @param modified <code>true</code> if the file was modified in the process scanning.
     */
    public ScanResult(final File file, final boolean clean, final boolean modified) {
        this.file = file;
        this.clean = clean;
        this.modified = modified;
    }

    /**
     * Create a new <code>ScanResult</code> Object. This Constructor assumes that
     * the file scanned was modified in the process.
     * @param file The file the scan was performed on
     * @param clean <code>true</code> if the file was clean, <code>false</code> otherwise.
     *
     */
    public ScanResult(final File file, final boolean clean) {
        this(file, clean, true);
    }

    /**
     * Returns true if the scanned file is free of viruses.
     * @return <code>true</code> if the file is free of viruses.<code>false</code>
     * otherwise.
     */
    public boolean isClean() {
        return clean;
    }

    /**
     * returns the file that was scanned.
     * @return The file object that was scanned.
     */
    public File getFile() {
        return file;
    }

    /**
     * Indicates if the file scanned was modified in the process.
     * @return <code>true</code> if the file was modified during the scan.
     * <code>false</code> otherwise.
     */
    public boolean isModified() {
        return modified;
    }
}
