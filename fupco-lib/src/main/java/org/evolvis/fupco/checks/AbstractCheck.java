/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.checks;

import java.io.File;
import java.util.*;

/**
 * @author Florian Wallner
 */
public abstract class AbstractCheck implements Check {

    protected Map<String, String> configurationProperties = new HashMap<String, String>();
    protected Set<String> requiredProperties = new HashSet<String>();
    protected CheckResult checkResult = new CheckResult();

    /**
     * {@inheritDoc}
     */
    @Override
    abstract public String getCheckDescription();

    /**
     * {@inheritDoc}
     */
    @Override
    abstract public CheckResult doCheck(File fileToCheck);

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getRequiredProperties() {
        return Collections.unmodifiableSet(requiredProperties);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProperty(String propertyName) {
        return configurationProperties.get(propertyName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CheckResult getCheckResult() {
        return checkResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Check withProperty(String key, String value) {
        if (configurationProperties.containsKey(key)) {
            configurationProperties.remove(key);
        }
        configurationProperties.put(key, value);
        return this;
    }

    /**
     * Checks if all required properties of the Check are set and if the file to check exists.
     *
     * @param fileToCheck The file this Check should be run against.
     * @return <code>true</code> if the Check is ready to run. <code>false</code> otherwise.
     */
    protected boolean isInitialized(File fileToCheck) {

        if (!fileToCheck.exists()) {
            checkResult = CheckResult.FAIL("File does not exist.", "");
            return false;
        }
        for (String reqKey : requiredProperties) {

            if (!configurationProperties.containsKey(reqKey)
                    || configurationProperties.get(reqKey).isEmpty()) {
                checkResult = CheckResult.FAIL("Property: " + reqKey
                        + " not set", reqKey);
                return false;
            }
        }
        return true;
    }
}
