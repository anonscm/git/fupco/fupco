/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.gwtclient.server;

import org.evolvis.fupco.UploadedFile;
import org.evolvis.fupco.Validator;
import org.evolvis.fupco.config.Configuration;
import org.evolvis.fupco.exceptions.ValidatorInitException;
import org.evolvis.fupco.gwtclient.shared.Verifiers;
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * request-handling
 */
public class FileUpload extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        /**
         * apache-stuff to extract the stream from the request
         */
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        /**
         * fupco-init
         */
        Configuration config;
        Validator validator = new Validator();
        try {
            config = Configuration.readConfig(getClass().getResource("/fupco-config.xml").getFile());
        } catch (ValidatorInitException vie) {
            throw new ServletException("could not configure fupco: " + vie.getMessage());
        }

        /**
         * extract the fields and validate them
         */
        try {
            /**
             * file
             */
            List<FileItem> items = upload.parseRequest(request);
            for (FileItem item : items) {
                if (item.isFormField()) {
                    if ("description".equals(item.getFieldName())) {
                        if (!Verifiers.isValid(item.getString())) {
                            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "description invalid, please use at least 4 characters.");
                            break;
                        }
                    }
                } else {
                    UploadedFile file = validator.validate(item.getInputStream(), config);
                    if (!file.isValidationSuccess()) {
                        response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE, "file validation failed.");
                    } else {
                        response.setStatus(HttpServletResponse.SC_CREATED);
                        response.getWriter().print("validation successful.");
                        response.flushBuffer();
                    }
                }
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}
