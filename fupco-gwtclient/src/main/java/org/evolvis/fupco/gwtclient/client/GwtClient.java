/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.gwtclient.client;

import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import org.evolvis.fupco.gwtclient.shared.Verifiers;

/**
 * ui
 */
public class GwtClient implements EntryPoint {

    /**
     * service for rpc call
     */
    private final CheckServiceAsync checkService = GWT.create(CheckService.class);

    public void onModuleLoad() {
        /**
         * form
         */
        final FormPanel form = new FormPanel();
        form.setEncoding(FormPanel.ENCODING_MULTIPART);
        form.setMethod(FormPanel.METHOD_POST);
        form.setAction(GWT.getModuleBaseURL() + "fileupload");

        /**
         * description textbox, upload widget and submit-Button
         */
        final TextBox description = new TextBox();
        final FileUpload upload = new FileUpload();
        final Button sendButton = new Button("SEND");

        /**
         *  be aware of the fact that only named items get submitted!
         */
        description.setName("description");
        upload.setName("upload");

        /**
         * a VerticalPanel to hold TextBox and FileUpload
         */
        VerticalPanel holder = new VerticalPanel();
        holder.add(description);
        holder.add(upload);
        holder.add(sendButton);

        /**
         * add the VerticalPanel to the FormPanel
         */
        form.setWidget(holder);

        /**
         * a label to inform the user about the current state
         */
        final Label info = new Label();

        /**
         * add the form and the info-label to the panel
         */
        RootPanel.get("formContainer").add(form);
        RootPanel.get("infoContainer").add(info);

        /**
         * the blurhandler-event on the description field fires onFocusLost
         * we use it to ask the server via RPC if the field value is valid
         * and to inform the user. this is not a smart way of validation.
         * better use gwtclient-side-validation as no rpc-call is needed here.
         * its just included to demonstrate RPC
         */
        description.addBlurHandler(new BlurHandler() {
            public void onBlur(BlurEvent event) {
                checkService.checkDescription(description.getText(), new AsyncCallback<String>() {
                    public void onSuccess(String message) {
                        info.setText(message);
                    }

                    public void onFailure(Throwable caught) {
                        info.setText(caught.getMessage());
                    }
                });
            }
        });

        /**
         * the forms submit-handler fires as soon as the form is submitted. its a good spot to do
         * some validation before we send the request to the server as we still can cancel the submit-event
         * be aware of the fact that this is gwtclient-side validation and the data still has to be validated
         * server-side as well
         */
        form.addSubmitHandler(new FormPanel.SubmitHandler() {
            public void onSubmit(FormPanel.SubmitEvent event) {
                if (!Verifiers.isValid(upload)) {
                    info.setText("choose a file!");
                    event.cancel();
                } else {
                    info.setText("all fine with file");
                }
            }
        });

        /**
         * this event is triggered AFTER the event has beed processed by the server
         * we can use it to inform the user whether the request was successful or not
         */
        form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
            public void onSubmitComplete(FormPanel.SubmitCompleteEvent event) {
                info.setText(event.getResults());
            }
        });

        /**
         * we need to register a click-event to the button to submit the form
         */
        sendButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.submit();
            }
        });
    }
}
