/**
 * File Upload Component
 *
 * The File Upload Component provides a simple and concise way to handle
 * the upload of files in web projects.
 *
 * Copyright (C) 2011-2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package org.evolvis.fupco.gwtclient.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import org.evolvis.fupco.gwtclient.shared.Verifiers;

public class GwtTestGwtClient extends GWTTestCase {

    /**
     * Must refer to a valid module that sources this class.
     */
    public String getModuleName() {
        return "org.evolvis.fupco.gwtclient.GwtClientJUnit";
    }

    /**
     * Tests the Verifiers.
     */
    public void testFieldVerifier() {
        assertFalse(Verifiers.isValid(""));
        assertFalse(Verifiers.isValid("a"));
        assertFalse(Verifiers.isValid("ab"));
        assertFalse(Verifiers.isValid("abc"));
        assertTrue(Verifiers.isValid("abcd"));
    }

    /**
     * This test will send a request to the server using checkDescription method in
     * CheckService and verify the response.
     */
    public void testCheckService() {
        // Create the service that we will test.
        CheckServiceAsync checkService = GWT.create(CheckService.class);
        ServiceDefTarget target = (ServiceDefTarget) checkService;
        target.setServiceEntryPoint(GWT.getModuleBaseURL() + "GwtClient/check");

        // Since RPC calls are asynchronous, we will need to wait for a response
        // after this test method returns. This line tells the test runner to wait
        // up to 10 seconds before timing out.
        delayTestFinish(10000);

        // Send a request to the server.
        checkService.checkDescription("GWTUser", new AsyncCallback<String>() {
            public void onFailure(Throwable caught) {
                // The request resulted in an unexpected error.
                fail("Request failure: " + caught.getMessage());
            }

            public void onSuccess(String result) {
                assertFalse(result.isEmpty());
                finishTest();
            }
        });
    }
}
